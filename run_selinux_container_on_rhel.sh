#!/bin/bash - 
#===============================================================================
#
#          FILE: run_selinux_container_on_rhel.sh
# 
#         USAGE: ./run_selinux_container.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Petr Lautrbach <plautrba@redhat.com>
#  ORGANIZATION: 
#       CREATED: 01/26/2017 23:24
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

docker run --privileged -v /sys/fs/selinux:/sys/fs/selinux:rw -v/etc/selinux:/etc/selinux:rw -v /etc/selinux:/var/lib/selinux:rw -it selinux-container-system:latest /usr/bin/bash


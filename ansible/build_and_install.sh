#!/bin/bash -x
#===============================================================================
#
#          FILE: build_and_install.sh
# 
#         USAGE: ./build_and_install.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Petr Lautrbach <plautrba@redhat.com>
#  ORGANIZATION: 
#       CREATED: 12/09/2016 09:19
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

if [ $# -gt 1 ]; then
    replace_rpms=$2
else
    replace_rpms="no"
fi

case $1 in
    "selinux")
        
        cd /usr/local/src/selinux.git
        
	if [ $replace_rpms = "replace_rpms" ]; then 
            make CFLAGS="`rpm --eval '%{optflags}'`" DESTDIR=/usr/local/selinux LIBDIR=/usr/local/selinux/usr/lib64 SHLIBDIR=/usr/local/selinux/lib64 install install-pywrap 2>&1 > /usr/local/src/build-selinux.log || make CFLAGS="`rpm --eval '%{optflags}'`" DESTDIR=/usr/local/selinux LIBDIR=/usr/local/selinux/usr/lib64 SHLIBDIR=/usr/local/selinux/lib64 install install-pywrap 2>&1 >> /usr/local/src/build-selinux.log
            
            export LD_LIBRARY_PATH=/usr/local/selinux/usr/lib64/:/usr/local/selinux/lib64/
        
            rpm -qa libsepol\* libselinux\* libsemanage\* policycoreutils\* checkpolicy\* secilc\* setools\* | sed 's/\(.*\)-[^-]*-[^-]*/\1/' | xargs rpm -e --nodeps
	fi
        
        make CFLAGS="`rpm --eval '%{optflags}'`" LIBDIR=/usr/lib64 SHLIBDIR=/lib64 install install-pywrap
        
        unset LD_LIBRARY_PATH
	;;

    "setools")

        cd /usr/local/src/setools.git
        git am /usr/local/src/0001-Fix-build.patch
        
	if [ $replace_rpms = "replace_rpms" ]; then 
            rpm -qa setools\* | sed 's/\(.*\)-[^-]*-[^-]*/\1/' | xargs rpm -e --nodeps
	fi

        python setup.py build 2>&1 > /usr/local/src/build-setools.log
        python setup.py install 2>&1 >> /usr/local/src/build-setools.log
	;;
esac


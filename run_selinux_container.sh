#!/bin/bash - 
#===============================================================================
#
#          FILE: run_selinux_container.sh
# 
#         USAGE: ./run_selinux_container.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Petr Lautrbach <plautrba@redhat.com>
#  ORGANIZATION: 
#       CREATED: 01/26/2017 23:24
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

type=$1
docker run --privileged -v /sys/fs/selinux:/sys/fs/selinux:rw -v/etc/selinux:/etc/selinux:rw -v /var/lib/selinux:/var/lib/selinux:rw -it selinux-container-$type:latest /usr/bin/bash

